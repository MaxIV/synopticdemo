from subprocess import Popen
from tango import Database, DevState
from taurus.qt.qtgui.panel import TaurusDevicePanel, TaurusForm
from taurus import tauruscustomsettings
from .popup import CommandsWidgetPopup


class ValvePopup(CommandsWidgetPopup):
    "Quick open/close for vacuum valves"
    commands = ("Open", DevState.OPEN), ("Close", DevState.CLOSE)


class ScreenPopup(CommandsWidgetPopup):
    "Quick movements for camera screens"
    commands = ("MoveIn", DevState.INSERT), ("MoveOut", DevState.EXTRACT)


class MotorPanel(TaurusForm):
    "A widget for displaying (a) sardana motor(s)"

    def __init__(self, *args, **kwargs):
        TaurusForm.__init__(self, *args, **kwargs)
        self.setCustomWidgetMap(
            getattr(tauruscustomsettings, "T_FORM_CUSTOM_WIDGET_MAP", {})
        )
        self.setWithButtons(False)

    def setModel(self, model):
        TaurusForm.setModel(self, model)


CLASS_PANELS = {
    "Motor": MotorPanel,
    "VacuumValve": ValvePopup,
    "PneumaticValve": ValvePopup,
    "CameraScreen": ScreenPopup,
    "Screen": ScreenPopup,
}


def get_panel(device):
    "Return an appropriate panel for the given device"
    db = Database()
    classname = db.get_class_for_device(device)
    print(f"Class {classname}")
    if classname in CLASS_PANELS:
        return CLASS_PANELS[classname]
    if classname == "Basler":
        camera_process(device)
        return
    return TaurusDevicePanel


def camera_process(cam):
    """Start a camera gui panel in its own process"""
    Popen(["luxviewer", cam])


if __name__ == "__main__":
    import sys
    from taurus.qt.qtgui.application import TaurusApplication

    qapp = TaurusApplication([])
    device = sys.argv[1]
    widget = get_panel(device)()
    widget.setModel(device)
    widget.show()
    qapp.exec_()
