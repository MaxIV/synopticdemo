# synopticdemo

This is a support material for the presentations done at the GUI workshop and the Tango workshop during ICALEPCS 2023. The presentations are accessible here:

[SVG Synoptic presentation - GUI workshop ICALEPCS 2023](https://indico.cern.ch/event/1299332/contributions/5617599/attachments/2729713/4744878/ICALEPCS%202023%20-%20GUI%20Workshop%20-%20MAX%20IV%20SVG%20Synoptic.pptx)

[SVG Synoptic presentation - Tango workshop ICALEPCS 2023](https://indico.tango-controls.org/event/55/contributions/790/attachments/524/685/ICALEPCS%202023%20-%20Tango%20Workshop%20-%20SVG%20Synoptic.pptx
)

(a pdf version is also accessible on the same sites)

## Requirements:

Packages:

* PyTango: https://gitlab.com/tango-controls/pytango
* Taurus: https://gitlab.com/taurus-org/taurus
* svgsynoptic2: https://gitlab.com/MaxIV/lib-maxiv-svgsynoptic

It also requires an accessible tango database with TangoTest device called `SYS/TG_TEST/1`

Tested with py3.9, taurus 5.1.6 and svgsynoptic2 4.2.2.

## Env:

With conda using conda-forge channel:

```bash
conda create -n synopticdemo -c conda-forge python=3.9 pytango=9.3.6 taurus svgsynoptic2
```

Info:

* svgsynoptic2: https://github.com/conda-forge/svgsynoptic2-feedstock
* Taurus: https://github.com/conda-forge/taurus-feedstock
* PyTango: https://github.com/conda-forge/pytango-feedstock

The packages are also available on PyPi:

* svgsynoptic2: https://pypi.org/project/svgsynoptic2/
* Taurus: https://pypi.org/project/taurus/
* PyTango: https://pypi.org/project/pytango/

So you can install with pip:

```bash
pip install pytango taurus svgsynoptic2
```

## Development

Install the synopticdemo package with local edit option:

```bash
pip install -e .
```

To launch:

```bash
ctsynopticdemo
```
